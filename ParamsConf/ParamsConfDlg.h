
// ParamsConfDlg.h : 头文件
//

#pragma once
#include "afxcmn.h"

// CParamsConfDlg 对话框
class CParamsConfDlg : public CDialogEx
{
// 构造
public:
	CParamsConfDlg(CWnd* pParent = NULL);	// 标准构造函数

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_PARAMSCONF_DIALOG };
#endif

	BOOL CheckFormat(CString value, int nRow, int nCol);
	void SetCell(HWND hWnd1, CString value, int nRow, int nCol);
	CString GetItemText(HWND hWnd, int nItem, int nSubItem) const;

	int nItem, nSubItem;
	CString StrCache;
	CString csvfilename;

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	void InitList();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButtonImport();
	CListCtrl m_ctrlList1;
	afx_msg void OnClickList1(NMHDR *pNMHDR, LRESULT *pResult);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnBnClickedButtonCatbin();
	afx_msg void OnBnClickedButtonExport();
};
