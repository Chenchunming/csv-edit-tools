
// ParamsConfDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "ParamsConf.h"
#include "ParamsConfDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CParamsConfDlg 对话框



CParamsConfDlg::CParamsConfDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_PARAMSCONF_DIALOG, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_ICON1);
}

void CParamsConfDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST1, m_ctrlList1);
}

BEGIN_MESSAGE_MAP(CParamsConfDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON_IMPORT, &CParamsConfDlg::OnBnClickedButtonImport)
	ON_NOTIFY(NM_CLICK, IDC_LIST1, &CParamsConfDlg::OnClickList1)
	ON_BN_CLICKED(IDC_BUTTON_CATBIN, &CParamsConfDlg::OnBnClickedButtonCatbin)
	ON_BN_CLICKED(IDC_BUTTON_EXPORT, &CParamsConfDlg::OnBnClickedButtonExport)
END_MESSAGE_MAP()


// CParamsConfDlg 消息处理程序

BOOL CParamsConfDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。  当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码
	InitList();
	::ShowWindow(::GetDlgItem(m_hWnd, IDC_EDIT1), SW_HIDE);

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CParamsConfDlg::InitList()
{
	//删除所有项
	m_ctrlList1.DeleteAllItems();
	//删除所有列
	while (m_ctrlList1.DeleteColumn(0) == TRUE) {
	};
	DWORD dwStyle = m_ctrlList1.GetExtendedStyle();
	dwStyle |= LVS_EX_FULLROWSELECT;
	dwStyle |= LVS_EX_GRIDLINES;//网格线
	m_ctrlList1.SetExtendedStyle(dwStyle); //设置扩展风格

	//创建列
	m_ctrlList1.InsertColumn(1, _T(""), LVCFMT_LEFT, 40, -1);
	m_ctrlList1.InsertColumn(1, _T("Index"), LVCFMT_LEFT, 100, -1);
	m_ctrlList1.InsertColumn(2, _T("Name"), LVCFMT_LEFT, 250, -1);
	m_ctrlList1.InsertColumn(3, _T("Key"), LVCFMT_LEFT, 150, -1);
	nItem = 0;
	nSubItem = 0;
}


void CParamsConfDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。  对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CParamsConfDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CParamsConfDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

//this function will returns the item text depending on the item and SubItem Index
CString CParamsConfDlg::GetItemText(HWND hWnd, int nItem, int nSubItem) const
{
	LVITEM lvi;
	memset(&lvi, 0, sizeof(LVITEM));
	lvi.iSubItem = nSubItem;
	CString str;
	int nLen = 128;
	int nRes;
	do
	{
		nLen *= 2;
		lvi.cchTextMax = nLen;
		lvi.pszText = str.GetBufferSetLength(nLen);
		nRes = (int)::SendMessage(hWnd, LVM_GETITEMTEXT, (WPARAM)nItem,
			(LPARAM)&lvi);
	} while (nRes == nLen - 1);
	str.ReleaseBuffer();
	return str;
}

// This function set the text in the specified SubItem depending on the Row and Column values
void CParamsConfDlg::SetCell(HWND hWnd1, CString value, int nRow, int nCol)
{
	TCHAR     szString[256];
	wsprintf(szString, value, 0);

	//Fill the LVITEM structure with the values given as parameters.
	LVITEM lvItem;
	lvItem.mask = LVIF_TEXT;
	lvItem.iItem = nRow;
	lvItem.pszText = szString;
	lvItem.iSubItem = nCol;
	if (nCol > 0)
		//set the value of listItem
		::SendMessage(hWnd1, LVM_SETITEM, (WPARAM)0, (WPARAM)&lvItem);
	//else
		//Insert the value into List
		//ListView_InsertItem(hWnd1, &lvItem);

}

void CParamsConfDlg::OnBnClickedButtonImport()
{
	// TODO: 在此添加控件通知处理程序代码
	CString line;
	CStdioFile csvfile;
	CString buffer;
	CString comma = _T(",");
	char *save_ptr;
	int i;

	CFileDialog opendlg(TRUE, _T("*"), _T("*.csv"), OFN_OVERWRITEPROMPT, _T("所有文件(*.*;)|*.*||"), NULL);
	if (opendlg.DoModal() == IDOK)
	{
		csvfilename = opendlg.GetPathName();
		csvfilename.Replace(_T("\\"), _T("\\\\"));
	}
	else
		return;

	int itemcount = m_ctrlList1.GetItemCount();

	for (i = itemcount-1; i >= 0; i--) {
		m_ctrlList1.DeleteItem(i);
	}
	csvfile.Open(csvfilename, CFile::modeReadWrite, NULL);
	while (csvfile.ReadString(line)) {
		char *templine = (char*)line.GetBuffer(line.GetLength());
		//MessageBox((LPCTSTR)templine);
		char *index = strtok_s(templine, ",", &save_ptr);
		char *param = strtok_s(NULL, ",", &save_ptr);
		char *name = strtok_s(NULL, ",", &save_ptr);
		itemcount = m_ctrlList1.GetItemCount();

		buffer.Format(_T("%03d"), itemcount);
		m_ctrlList1.InsertItem(itemcount, _T(""));
		m_ctrlList1.SetItemText(itemcount, 0, buffer);

		CStringArray strResult;
		CString strGap = _T(",");
		int nPos = line.Find(strGap);
		CString strLeft = _T("");

		while (0 <= nPos)
		{
			strLeft = line.Left(nPos);
			if (!strLeft.IsEmpty())
				strResult.Add(strLeft);
			line = line.Right(line.GetLength() - nPos - 1);
			nPos = line.Find(strGap);
		}
		if (!line.IsEmpty()) {
			strResult.Add(line);
		}

		int nSize = strResult.GetSize();
		m_ctrlList1.SetItemText(itemcount, 1, strResult.GetAt(0));
		if (nSize >= 3)
			m_ctrlList1.SetItemText(itemcount, 2, strResult.GetAt(2));
		if (nSize >= 2)
			m_ctrlList1.SetItemText(itemcount, 3, strResult.GetAt(1));
	}
	csvfile.Close();
}



void CParamsConfDlg::OnClickList1(NMHDR *pNMHDR, LRESULT *pResult)
{
	CString xstr;
	Invalidate();
	LPNMITEMACTIVATE pNMItemActivate = (LPNMITEMACTIVATE)pNMHDR;
	HWND hWnd1 = ::GetDlgItem(m_hWnd, IDC_LIST1);
	RECT rect;

	//get the text from the EditBox
	GetDlgItemText(IDC_EDIT1, xstr);
	if (nSubItem != 0 && nSubItem != 1 && CheckFormat(xstr, nItem, nSubItem)) {
		::SetWindowText(::GetDlgItem(m_hWnd, IDC_EDIT1), xstr);
		//set the value in the listContorl with the specified Item & SubItem values
		SetCell(::GetDlgItem(m_hWnd, IDC_LIST1), xstr, nItem, nSubItem);
		::SendDlgItemMessage(m_hWnd, IDC_EDIT1, WM_KILLFOCUS, 0, 0);
		::ShowWindow(::GetDlgItem(m_hWnd, IDC_EDIT1), SW_HIDE);
	}
	else if(nSubItem != 0 && nSubItem != 1 && !CheckFormat(xstr, nItem, nSubItem)){
		MessageBox(_T("Invalid Format!"), _T("Error"), MB_ICONERROR | MB_OK);
		//set the value in the listContorl with the specified Item & SubItem values
		::SetWindowText(::GetDlgItem(m_hWnd, IDC_EDIT1), StrCache);
		SetCell(::GetDlgItem(m_hWnd, IDC_LIST1), StrCache, nItem, nSubItem);
		::SendDlgItemMessage(m_hWnd, IDC_EDIT1, WM_KILLFOCUS, 0, 0);
		::ShowWindow(::GetDlgItem(m_hWnd, IDC_EDIT1), SW_HIDE);
	}
	else {}
	//get the row number
	nItem = pNMItemActivate->iItem;
	//get the column number
	nSubItem = pNMItemActivate->iSubItem;
	if (nSubItem == 0 || nSubItem == 1 || nSubItem == -1 || nItem == -1)
	{
		nItem = 0;
		nSubItem = 0;
		return;
	}
	//Retrieve the text of the selected subItem from the list
	StrCache = GetItemText(hWnd1, nItem, nSubItem);

	RECT rect1, rect2;
	// this macro is used to retrieve the Rectanle of the selected SubItem
	ListView_GetSubItemRect(hWnd1, pNMItemActivate->iItem, pNMItemActivate->iSubItem, LVIR_BOUNDS, &rect);
	//Get the Rectange of the listControl
	::GetWindowRect(pNMItemActivate->hdr.hwndFrom, &rect1);
	//Get the Rectange of the Dialog
	::GetWindowRect(m_hWnd, &rect2);

	int x = rect1.left - rect2.left;
	int y = rect1.top - rect2.top;
	if (nItem != -1)
		::SetWindowPos(::GetDlgItem(m_hWnd, IDC_EDIT1), HWND_TOP, x - 6 + rect.left, 20 + rect.top, rect.right - rect.left, rect.bottom - rect.top, NULL);
	::ShowWindow(::GetDlgItem(m_hWnd, IDC_EDIT1), SW_SHOW);
	::SetFocus(::GetDlgItem(m_hWnd, IDC_EDIT1));
	//Draw a Rectangle around the SubItem
	::Rectangle(::GetDC(pNMItemActivate->hdr.hwndFrom), rect.left, rect.top - 1, rect.right, rect.bottom);
	//Set the listItem text in the EditBox
	::SetWindowText(::GetDlgItem(m_hWnd, IDC_EDIT1), StrCache);
	SetCell(::GetDlgItem(m_hWnd, IDC_LIST1), StrCache, nItem, nSubItem);
	*pResult = 0;
}


BOOL CParamsConfDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: 在此添加专用代码和/或调用基类
	if (pMsg->message == WM_KEYDOWN && pMsg->wParam == VK_ESCAPE)// 屏蔽esc键  
	{
		return TRUE;// 不作任何操作  
	}
	if (pMsg->message == WM_KEYDOWN && pMsg->wParam == VK_RETURN)// 屏蔽enter键  
	{
		CWnd* pwndCtrl = GetFocus();
		// get the control ID which is presently having the focus
		int ctrl_ID = pwndCtrl->GetDlgCtrlID();
		CString str;
		switch (ctrl_ID)
		{	//if the control is the EditBox	
		case IDC_EDIT1:
			//get the text from the EditBox
			GetDlgItemText(IDC_EDIT1, str);
			if (CheckFormat(str, nItem, nSubItem)) {
				::SetWindowText(::GetDlgItem(m_hWnd, IDC_EDIT1), str);
			}
			else {
				MessageBox(_T("Invalid Format!"), _T("Error"), MB_ICONERROR | MB_OK);
				::SetWindowText(::GetDlgItem(m_hWnd, IDC_EDIT1), StrCache);
			}
			//set the value in the listContorl with the specified Item & SubItem values
			GetDlgItemText(IDC_EDIT1, str);
			SetCell(::GetDlgItem(m_hWnd, IDC_LIST1), str, nItem, nSubItem);
			::SendDlgItemMessage(m_hWnd, IDC_EDIT1, WM_KILLFOCUS, 0, 0);
			::ShowWindow(::GetDlgItem(m_hWnd, IDC_EDIT1), SW_HIDE);
			break;
		default:
			break;
		}
		return TRUE;// 不作任何处理  
	}
	return CDialogEx::PreTranslateMessage(pMsg);
}


BOOL CParamsConfDlg::CheckFormat(CString value, int nRow, int nCol)
{
	// TODO: 在此添加专用代码和/或调用基类
	CString strTemp;
		
	switch (nCol)
	{
	case 0:
		return FALSE;
	case 1:
		return FALSE;
	case 2:
		return TRUE;
	case 3:
		strTemp = value.SpanIncluding(_T("0123456789"));
		if (value.GetLength() == strTemp.GetLength()) {
			unsigned int temp = _ttoi(strTemp);
			if(temp > 65535)
				return FALSE;
			return TRUE;
		}
		else {
			CString sFirst = value.Left(2);
			if (sFirst.CompareNoCase(_T("0x")) == 0 || sFirst.CompareNoCase(_T("0X")) == 0)
			{
				CString sLast = value.Right(value.GetLength() - 2);
				strTemp = sLast.SpanIncluding(_T("ABCDEFabcdef0123456789"));
				if (sLast.GetLength() == strTemp.GetLength()) {
					if (sLast.GetLength() > 4)
						return FALSE;
					return TRUE;
				}
				else {
					return FALSE;
				}
			}
			else {
				return FALSE;
			}
		}
	default:
		return FALSE;
	}
}


void CParamsConfDlg::OnBnClickedButtonCatbin()
{
	CString binfilename, jointbinfilename;
	UINT confsize, appsize;
	STARTUPINFO si = { sizeof(si) };
	PROCESS_INFORMATION pi;

	if (csvfilename.IsEmpty()) {
		MessageBox(_T("No parameters to proecess!"));
		return;
	}

	CFileDialog opendlg(TRUE, _T("*"), _T("*.bin"), OFN_OVERWRITEPROMPT, _T("Application binary(*.bin)|*.bin||"), NULL);
	if (opendlg.DoModal() == IDOK)
	{
		binfilename = opendlg.GetPathName();
		binfilename.Replace(_T("\\"), _T("/"));
	}
	else
		return;

	CString strCmd = _T("component/csv2bin.exe ") + csvfilename + _T(" component/conf.bin");
	if (!CreateProcess(NULL, (LPWSTR)strCmd.GetString(), NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi)) {
		MessageBox(_T("Transfer csv to bin Fail!"));
		DeleteFile(_T("component/conf.bin"));
		return;
	}
	WaitForSingleObject(pi.hProcess, INFINITE);
	CloseHandle(pi.hThread);
	CloseHandle(pi.hProcess);

	CFileStatus fileStatus;
	CStdioFile appbin;
	appbin.Open(binfilename, CFile::modeRead | CFile::typeBinary, NULL);
	CStdioFile confbin;
	confbin.Open(_T("component/conf.bin"), CFile::modeRead | CFile::typeBinary, NULL);
	CStdioFile outputbin;
	outputbin.Open(_T("component/output.bin"), CFile::modeCreate | CFile::modeReadWrite | CFile::typeBinary, NULL);

	if (confbin.GetStatus(fileStatus))	confsize = (UINT)fileStatus.m_size;
	if (appbin.GetStatus(fileStatus))	appsize = (UINT)fileStatus.m_size;

	UINT uRead;
	byte *buff = new byte[1024];
	byte *zeros = new byte[131072];
	memset(zeros, 0, 131072);
	do
	{
		uRead = confbin.Read(buff, 1024);
		outputbin.Write(buff, uRead);
	} while (uRead>0);
	outputbin.Write(zeros, 16384 - confsize);

	do
	{
		uRead = appbin.Read(buff, 1024);
		outputbin.Write(buff, uRead);
	} while (uRead>0);
	outputbin.Write(zeros, 131072 - appsize);

	appbin.Close();
	confbin.Close();
	outputbin.Close();

	CFileDialog savedlg(FALSE, _T("*"), _T("*.bin"), OFN_OVERWRITEPROMPT, _T("Joint binary(*.bin;)|*.bin||"));
	if (savedlg.DoModal() == IDOK) {
		jointbinfilename = savedlg.GetPathName();
		jointbinfilename.Replace(_T("\\"), _T("/"));
	}
	else {
		DeleteFile(_T("component/conf.bin"));
		DeleteFile(_T("component/output.bin"));
		return;
	}

	BOOL retcopyfile = CopyFile(_T("component/output.bin"), jointbinfilename, TRUE);
	if (!retcopyfile)
		MessageBox(_T("File exist, export error!"));
	DeleteFile(_T("component/conf.bin"));
	DeleteFile(_T("component/output.bin"));
}


void CParamsConfDlg::OnBnClickedButtonExport()
{
	//格式：过滤器描述符（显示作用）+ \0 + 文件扩展名称（过滤作用）  
	//多个扩展名称之间用（;）分隔，两个过滤字符串之间以\0分隔  
	//最后的过滤器需要以两个\0\0结尾  
	CStdioFile csvfile;
	CString StrCache;
	int itemcount = m_ctrlList1.GetItemCount();
	int nItem;
	CString szFilters = _T("Comma-Separated Values File(*.csv)\0*.csv\0" \
		"All Typle(*.*)\0*.*\0" \
		"\0");
	//当过滤器或者默认构造参数赋值较少情况下，  
	//使用构造函数修改对话框初始状态可能更好，这过滤器较多  
	CFileDialog savedlg(FALSE, _T("*"), _T("*.csv"), OFN_OVERWRITEPROMPT, szFilters);

	if (IDOK == savedlg.DoModal())
	{
		csvfilename = savedlg.GetPathName();
		csvfile.Open(csvfilename, CFile::modeCreate | CFile::modeReadWrite, NULL);

		for (nItem = 0; nItem < itemcount; nItem++) {
			StrCache = m_ctrlList1.GetItemText(nItem, 1);
			csvfile.WriteString(StrCache);
			csvfile.WriteString(_T(","));
			StrCache = m_ctrlList1.GetItemText(nItem, 3);
			csvfile.WriteString(StrCache);
			csvfile.WriteString(_T(","));
			StrCache = m_ctrlList1.GetItemText(nItem, 2);
			csvfile.WriteString(StrCache);
			csvfile.WriteString(_T("\n"));
		}

		//立即写入，不缓冲  
		csvfile.Flush();
		//文件操作结束关闭  
		csvfile.Close();
	}
}
