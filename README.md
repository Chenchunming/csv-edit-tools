## 基于CSV修改参数生成BIN文档的工具组

### Usage：
- 1.导入csv文件
- 2.点击表格进行编辑
- 3.导出csv文件可用于存档
- 4.生成BIN文档时选择应用程序BIN档，合成的BIN另外保存

### v0.1 release
	Date: Feb.11, 2018
	Author: Chenchunming
	Changelog:
		1.修改生成bin文件功能，移除大部分外部调用进程
		2.移除外部工具HBIN.exe和相关进程